# Taller: Primero fue la línea de comandos

#### Herramientas
[Online Linux Terminal](https://cocalc.com/doc/terminal.html)

o la terminal de su equipo con sistema operativo base Linux o Unix.

#### Editor de texto
nano

#### Permisos de ejecución
chmod u+xr file.sh

#### Para ejecutar un script
./file.sh

#### Video explicativo
[Enlace YouTube](https://www.youtube.com/watch?v=3eC4lBjHNz8&t=13s)

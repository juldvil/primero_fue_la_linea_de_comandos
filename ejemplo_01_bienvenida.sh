#!/bin/bash

clear

echo
echo "Bienvenidos al taller:"
echo "Primero_fue_la_linea_de_comandos.sh"
echo

echo
echo "Ahora estamos trabajando en el directorio:"
pwd

echo
echo "Creando nuevo directorio"
mkdir wise

echo
echo "Entrando al nuevo directorio"
cd wise

echo
echo "Ahora está en el directorio:"
pwd

echo
echo "Creando un archivo de texto"
cat > hola_mundo.txt <<EOF
Hola, esto es un mensaje de bienvenida.
Espero que este taller sea de su agrado.
EOF

echo
echo "Listando archivos dentro del directorio"
ls

exit 0

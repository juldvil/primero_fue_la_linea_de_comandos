#!/bin/bash

clear


numero_entero=100
numero_decimal=2.5

un_caracter="A"
letras="String de caracteres"


echo "Imprimir variables:"
echo
echo "la variable numero_entero tiene un valor de: $numero_entero"
echo
echo "La variable numero_decimal tiene un valor de: $numero_decimal"
echo
echo "La variable un carácter tiene un valor de: $un_caracter"
echo
echo "La variables letras tiene un valor de: $letras"
echo


exit 0

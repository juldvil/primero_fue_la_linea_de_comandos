#!/bin/bash

clear


echo
echo "Ejemplo 1"
echo

for i in {1..5}
do
    echo "Número: $i"
done



echo
echo "Ejemplo 2"
echo

for i in 1 2 3 4 5
do
    echo "Número: $i"
done


echo
echo "Ejemplo 3"
echo

for i in {0..10..2}
do
    echo "Número: $i"
done


echo
echo "Ejemplo 4"
echo

for directorios in wise_1 wise_2 wise_3
do
    echo "Crear directorio $directorios"
    mkdir $directorios
done


echo
echo "Ejemplo 5"
echo

for i in 4 5 6
do
    echo "Crear directorio directorios_$i"
    mkdir wise_$i
done


echo
echo "Ejemplo 6"
echo

directorios=`ls -d */`

for carpeta in $directorios
do
    cd $carpeta
    echo
    echo
    echo
    echo "Ahora estamos en el directorio $carpeta"
    touch mensaje.txt
    echo "Saludo a los asistentes" >> mensaje.txt
    echo
    pwd
    cd ..
done


exit 0

#!/bin/bash


clear


funcion1()
{
    echo
    echo "Esta es la función 1"
    echo
}


funcion2()
{
    echo
    echo "Esta es la función 2"
    echo
}


funcion3()
{
    echo
    echo "Esta es la función 3"
    echo
}


echo
echo "================================================================================"
echo "=                            Eliga una opción                                  ="
echo "================================================================================"
echo "Digite:    1 = Función 1,       2 = Función 2,        3 = Función 3            ="
echo "================================================================================"
echo
read funcion


case $funcion in
    1)
        funcion1
    ;;
    2)
        funcion2
    ;;
    3)
        funcion3
    ;;
esac


exit 0
